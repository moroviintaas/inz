(*==========================================================================*
|	Protokół IKE v2, 														|
|	INITIAL Exchange - 	wersja z uwierzytelnianiem przez podpis inicjatora	|
|						MAC odpowiadającego; w IKE_AUTH dodatkowe idr		|
|	Przeciwnik:			BR													|
|	Data aktualizacji:	2018-01-15											|
|	Wykonał:			sierż. pchor. Robert Jarosz							|
|	Organizacja:		WAT, WCY, IMIK 										|
===========================================================================*)


type spi.
type key.
type agent.
type num.
type SA.
type nonce.
type traffic_selector.
type hkey.
type password.
type spkey.
type sskey.

type atck.
const br:atck.

(* Grupa na potrzeby Diffiego-Hellmana *)
type G.
type exponent.
const g: G.
fun exp(G, exponent) : G.
equation forall  x: exponent, y: exponent; exp(exp(g,x),y) = exp(exp(g,y),x).


(*szyfrowanie symetryczne *)
fun sencrypt(bitstring, key) : bitstring.
reduc forall m : bitstring, k : key; sdecrypt(sencrypt(m,k),k) = m.

(*podpisy cyfrowe*)
fun spk(sskey): spkey.
fun sign(bitstring, sskey): bitstring.
reduc forall m: bitstring, k: sskey; getmess(sign(m,k)) = m.
reduc forall m: bitstring, k: sskey; checksign(sign(m,k), spk(k)) = m.

(*funkcje wykorzystywane w protokole *)
fun SK_ei(nonce, nonce, G, spi, spi) : key.
fun SK_er(nonce, nonce, G, spi, spi) : key.
fun SK_pi(nonce, nonce, G, spi, spi) : key.
fun SK_pr(nonce, nonce, G, spi, spi) : key.
fun prf(key, agent) : bitstring.
fun auth_mac(agent, agent, spi, spi, num, SA, G, nonce, nonce, bitstring) : bitstring.

(* Funkcja MAC i generowania klucza *)
fun hashkey(password, G) : hkey.
fun MAC (hkey,bitstring) : bitstring.
(*fun MAC (bitstring) : bitstring.*)


free zero:num.
free one:num.
const undef_spi : spi.

free hk:hkey [private].

free c : channel. 
free ec : channel.
free sc1 : channel [private].
free sc2 : channel [private].
free secret : bitstring [private].
free exec:bitstring [private].   
free cc: channel.
(*free shared_secret : password [private].*)
(*const ss : bitstring [private].*)

event Running_I(agent, agent, G).
event Running_R(agent, agent, G).
event Commit_I(agent, agent, G).
event Commit_R(agent, agent, G).
event Running(agent,agent, nonce).
event Commit(agent, agent, nonce).
event Attack(atck, G).
(*executability*)
(*query attacker(exec).*)
(*secrecy*)
(*query attacker(secret).*)
(*injective agree IR*)
query x: agent, y: agent, z: G; inj-event(Commit_I(x,y,z)) ==> inj-event(Running_R(x,y,z))  ||  event(Attack(br, z)).
(*injective agree RI*)
query x: agent, y: agent, z: G; inj-event(Commit_R(x,y,z)) ==> inj-event(Running_I(x,y,z))  ||  event(Attack(br, z)).
(*alive IR *)
query x: agent, y:agent,a:agent, n1:nonce, n2:nonce, z:G; event(Commit(x,y,n1)) ==> ((event(Running(a,y,n2)) || event(Running(y,a,n2))) && n1 <> n2)  ||  event(Attack(br, z)). 

table secrets(agent,agent, password).
free A : agent.     
free B : agent.
table keys(agent, spkey).

let initiator(pkCA : spkey, skA : sskey, skB : sskey)= 
	(* Sprawdzenie czy użytkownik jest uczciwy - czy ma dostęp do prywatnego
	klucza A lub B *)
	in(c, (id_I : agent, id_R : agent));
	if id_I = A || id_I = B then
	let I = id_I in
	let R = id_R in
	let skI = if id_I = A then skA else skB in
	let pkI = spk(skI) in
	
	
	new SA1 : SA;
	new Ni : nonce;
	new SPI_i : spi;
	new ei : exponent;
	let KE_i = exp(g,ei) in
	out(c, (I, R, SPI_i, undef_spi, zero, SA1, KE_i, Ni)); 
	in (c, (=I, =R, =SPI_i, SPI_r : spi, =zero, =SA1, KE_r : G, Nr : nonce) );
	new TSi : traffic_selector;
	new TSr : traffic_selector;
	new SA2 : SA;
	let DH_secret = exp(KE_r, ei) in 
	let sk_ei = SK_ei(Ni, Nr, DH_secret, SPI_i, SPI_r) in
	let sk_er = SK_er(Ni, Nr, DH_secret, SPI_i, SPI_r) in
	get secrets(=I,=R, shared_secret) in
	let hk = hashkey(shared_secret, DH_secret) in
	let auth_prf_i = prf(SK_pi(Ni, Nr, DH_secret, SPI_i, SPI_r), I) in
	let auth_prf_r = prf(SK_pr(Ni, Nr, DH_secret, SPI_i, SPI_r), R) in
	let auth_payload_i = sign(( SPI_i, undef_spi, zero, SA1, KE_i, Ni, Nr, auth_prf_i), skI) in
	let enc_i = sencrypt((I,R, auth_payload_i,pkI, SA2, TSi, TSr), sk_ei) in
	event Running_I(I,R, DH_secret);
	event Running(I,R,Ni);
	out(c, (I,R,SPI_i, SPI_r, one, enc_i)); 
	
	in (c, (=I,=R, =SPI_i, =SPI_r, =one, enc_r: bitstring));
	let (=R, auth_payload_r: bitstring,  =SA2, =TSi, =TSr) = sdecrypt (enc_r, sk_er) in
	
	let (=auth_payload_r) = MAC(hk,(SPI_i, SPI_r, zero, SA1, KE_r,  Nr, Ni, auth_prf_r)) in
	
	event Commit_I(I,R,DH_secret);
	event Commit(I,R, Ni);
	
	in(cc, md : atck);
	new sec : bitstring;
	out(c,(sencrypt(secret, sk_ei),exec));
	if md = br then
		event Attack(br, DH_secret);
		out(cc, (ei));
	
	
	
	0.
	
let responder(pkCA : spkey, skA : sskey, skB : sskey)= 

	in(c,  id_R : agent);
	if id_R = A || id_R = B then
	let R = id_R in
	
	
	in(c, (I:agent, =R, SPI_i: spi, =undef_spi, =zero, SA1:SA, KE_i:G, Ni:nonce)); 
	new Nr : nonce;
	new SPI_r: spi;
	new er : exponent;
	let KE_r = exp(g, er) in
	out(c,(I,R,SPI_i, SPI_r, zero, SA1, KE_r, Nr)); 
	in (c, (=I,=R,=SPI_i, =SPI_r, =one, enc_i : bitstring));
	let DH_secret = exp(KE_i, er) in 
	let sk_ei = SK_ei(Ni, Nr, DH_secret, SPI_i, SPI_r) in
	let sk_er = SK_er(Ni, Nr, DH_secret, SPI_i, SPI_r) in
	get secrets(=I,=R, shared_secret) in
	let hk = hashkey(shared_secret, DH_secret) in
	let auth_prf_i = prf(SK_pi(Ni, Nr, DH_secret, SPI_i, SPI_r), I) in
	let auth_prf_r = prf(SK_pr(Ni, Nr, DH_secret, SPI_i, SPI_r), R) in
	let (=I, =R, auth_payload_i:bitstring,pkI:spkey, SA2 : SA, TSi: traffic_selector, TSr: traffic_selector) = sdecrypt(enc_i, sk_ei) in

	(*let (=auth_payload_i) = MAC(hk,(I,R,SPI_i, undef_spi, zero, SA1, KE_i, Ni, Nr, auth_prf_i)) in*)
	
	
	out(c, (R,I));
	in (c, crt:bitstring);
	
	let (=pkI, =I) = checksign(crt,pkCA) in
	
	let ( =SPI_i, =undef_spi, =zero, =SA1,  =KE_i, =Ni, =Nr, =auth_prf_i) = checksign(auth_payload_i,pkI) in
	

	let auth_payload_r = MAC(hk,(SPI_i, SPI_r, zero, SA1, KE_r,  Nr, Ni, auth_prf_r)) in
	
	let enc_r = sencrypt((R,auth_payload_r, SA2, TSi, TSr), sk_er) in
	
	event Running_R(I,R,DH_secret);
	event Running(I,R, Nr);
	out (c, (I,R,SPI_i, SPI_r, one, enc_r));

	event Commit_R(I,R,DH_secret);
	
	event Commit(I,R, Nr);
	in(cc, md : atck);
	event Attack(br, DH_secret);
	out(cc, (er));
	0.

(* CA *)

let processCA(skCA: sskey) =  
        in(c,(a: agent, b: agent)); 
	get keys(=b, sb) in
        out(c,sign((sb,b),skCA)).

(* Key registration *)

let processK =
        in(c, (h: agent, k: spkey));
        if h <> A && h <> B then insert keys(h,k).
        

let processfA =
        new fa:agent;
        new k:password; 
        insert secrets(A,fa,k);
        insert secrets(fa,A,k);
        out(c,fa).
        
let processfB =
        new fa:agent;
        new k:password;
        insert secrets(B,fa,k);
        insert secrets(fa,B,k);
        out(c,fa).
	
process
	new skCA : sskey;
	let pkCA = spk(skCA) in
	new skA	 : sskey;
	let pkA = spk(skA) in
		out(c,pkA);
	insert keys(A, pkA);
	
	new skB :sskey;
	let pkB = spk(skB) in
		out(c, pkB);
	insert keys(B, pkB);
	
	new ss : password;
	insert secrets(A,B,ss);
	insert secrets(B,A,ss);
	out(c, pkCA);
	(
	 (!initiator(pkCA, skA, skB)) |
	 (!responder(pkCA, skA, skB) ) | 
	 (!processCA(skCA)) 
	 |( !processfA)
	 |( !processfB)
	)
	