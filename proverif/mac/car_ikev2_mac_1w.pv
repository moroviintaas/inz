(*==========================================================================*
|	Protokół IKE v2, 														|
|	INITIAL Exchange - 	wersja z uwierzytelnianiem przez MAC po obu stronach|
|						sekrety jednokierunkowe, różne klucze szyfrujące	|
|	Przeciwnik:			CA - responder										|
|	Data aktualizacji:	2018-01-15											|
|	Wykonał:			sierż. pchor. Robert Jarosz							|
|	Organizacja:		WAT, WCY, IMIK 										|
===========================================================================*)
type spi.
type key.
type agent.
type num.
type SA.
type nonce.
type traffic_selector.
type hkey.
type password.




(* Grupa na potrzeby Diffiego-Hellmana *)
type G.
type exponent.
const g: G.
fun exp(G, exponent) : G.
equation forall  x: exponent, y: exponent; exp(exp(g,x),y) = exp(exp(g,y),x).


(*szyfrowanie symetryczne *)
fun sencrypt(bitstring, key) : bitstring.
reduc forall m : bitstring, k : key; sdecrypt(sencrypt(m,k),k) = m.

(*funkcje wykorzystywane w protokole *)
fun SK_ei(nonce, nonce, G, spi, spi) : key.
fun SK_er(nonce, nonce, G, spi, spi) : key.
fun SK_pi(nonce, nonce, G, spi, spi) : key.
fun SK_pr(nonce, nonce, G, spi, spi) : key.
fun prf(key, agent) : bitstring.
fun auth_mac(agent, agent, spi, spi, num, SA, G, nonce, nonce, bitstring) : bitstring.

(* Funkcja MAC i generowania klucza *)
fun hashkey(password, G) : hkey.
fun MAC (hkey,bitstring) : bitstring.
(*fun MAC (bitstring) : bitstring.*)


free zero:num.
free one:num.
const undef_spi : spi.

(*free hk:hkey [private].*)

free c : channel. 
free cc : channel.
free sc1 : channel [private].
free sc2 : channel [private].
free secret : bitstring [private].
free exec:bitstring [private].    
(*free shared_secret : password [private].*)
(*const ss : bitstring [private].*)

event Running_I(agent, agent, G).
event Running_R(agent, agent, G).
event Commit_I(agent, agent, G).
event Commit_R(agent, agent, G).
event Running(agent,agent, nonce).
event Commit_aI(agent, agent, nonce).
event Commit_aR(agent, agent, nonce).
event CA(agent).

(*executability*)

(*query attacker(exec).*)
(*secrecy*)
(*query attacker(secret).*)
(*injective agree IR*)
query x: agent, y: agent, z: G; inj-event(Commit_I(x,y,z)) ==> inj-event(Running_R(x,y,z)) || event(CA(y)).
(*injective agree RI*)
query x: agent, y: agent, z: G; inj-event(Commit_R(x,y,z)) ==> inj-event(Running_I(x,y,z)) || event(CA(y)).
(*alive IR *)
query x: agent, y:agent,a:agent, n1:nonce, n2:nonce; event(Commit_aI(x,y,n1)) ==> ((event(Running(a,y,n2)) || event(Running(y,a,n2))) && n1 <> n2) || event(CA(y)). 

query x: agent, y:agent,a:agent, n1:nonce, n2:nonce; event(Commit_aR(x,y,n1)) ==> ((event(Running(x,a,n2)) || event(Running(a,x,n2))) && n1 <> n2) || event(CA(y)). 

table secrets(agent,agent, password).

free A : agent.     
free B : agent.


let initiator(*(ss:bitstring)*) (*(I:agent, R:agent) *)= 
	(*Sprawdzenie czy inicjator jest uczciwy (czy jest albo A albo B - 
	uprawnionym do znania shared_secret *)
	in(c, (id_I : agent, id_R : agent));
	if id_I = A  || id_I = B  then
	let I = id_I in
	let R = id_R in
	(*Właściwy protokół *)
	new SA1 : SA;
	new Ni : nonce;
	new SPI_i : spi;
	new ei : exponent;
	let KE_i = exp(g,ei) in
	out(c, (I, R, SPI_i, undef_spi, zero, SA1, KE_i, Ni)); 
	in (c, (=I, =R, =SPI_i, SPI_r : spi, =zero, =SA1, KE_r : G, Nr : nonce) );
	new TSi : traffic_selector;
	new TSr : traffic_selector;
	new SA2 : SA;
	let DH_secret = exp(KE_r, ei) in 
	let sk_ei = SK_ei(Ni, Nr, DH_secret, SPI_i, SPI_r) in
	let sk_er = SK_er(Ni, Nr, DH_secret, SPI_i, SPI_r) in
	get secrets(=I,=R, shared_secretIR) in
	get secrets(=R,=I, shared_secretRI) in
	let hkIR = hashkey(shared_secretIR, DH_secret) in
	let hkRI = hashkey(shared_secretRI, DH_secret) in
	
	let auth_prf_i = prf(SK_pi(Ni, Nr, DH_secret, SPI_i, SPI_r), I) in
	let auth_prf_r = prf(SK_pr(Ni, Nr, DH_secret, SPI_i, SPI_r), R) in
	let auth_payload_i = MAC(hkIR,( SPI_i, undef_spi, zero, SA1, KE_i, Ni, Nr, auth_prf_i)) in
	let enc_i = sencrypt((I,auth_payload_i, SA2, TSi, TSr), sk_ei) in
	event Running_I(I,R, DH_secret);
	event Running(I,R,Ni);
	out(c, (I,R,SPI_i, SPI_r, one, enc_i)); 
	
	in (c, (=I,=R, =SPI_i, =SPI_r, =one, enc_r: bitstring));
	let (=R, auth_payload_r: bitstring, =SA2, =TSi, =TSr) = sdecrypt (enc_r, sk_er) in
	let (=auth_payload_r) = MAC(hkRI,(SPI_i, SPI_r, zero, SA1, KE_r,  Nr,Ni, auth_prf_r)) in
	
	event Commit_I(I,R,DH_secret);
	event Commit_aI(I,R, Ni);
	out(c,(sencrypt(secret, sk_ei), exec));
	
	0.
	
let responder(*(ss:bitstring)*)(*( I:agent, R:agent) *)= 
	(*Sprawdzenie czy inicjator jest uczciwy (czy jest albo A albo B - 
	uprawnionym do znania shared_secret *)
	in(c, (id_R : agent));
	if id_R = A || id_R = B then
	let R = id_R in
	(*Właściwy protokół *)
	in(c, (I : agent, =R, SPI_i: spi, =undef_spi, =zero, SA1:SA, KE_i:G, Ni:nonce)); 
	(*if(I =A || I = B) && I <>R then*)
	new Nr : nonce;
	new SPI_r: spi;
	new er : exponent;
	let KE_r = exp(g, er) in
	out(c,(I,R,SPI_i, SPI_r, zero, SA1, KE_r, Nr)); 
	in (c, (=I,=R,=SPI_i, =SPI_r, =one, enc_i : bitstring));
	let DH_secret = exp(KE_i, er) in 
	let sk_ei = SK_ei(Ni, Nr, DH_secret, SPI_i, SPI_r) in
	let sk_er = SK_er(Ni, Nr, DH_secret, SPI_i, SPI_r) in
	get secrets(=I,=R, shared_secretIR) in
	get secrets(=R,=I, shared_secretRI) in
	let hkIR = hashkey(shared_secretIR, DH_secret) in
	let hkRI = hashkey(shared_secretRI, DH_secret) in
	out(cc,shared_secretIR);
	in(cc, ());
	event CA(R);
	let auth_prf_i = prf(SK_pi(Ni, Nr, DH_secret, SPI_i, SPI_r), I) in
	let auth_prf_r = prf(SK_pr(Ni, Nr, DH_secret, SPI_i, SPI_r), R) in
	let (=I, auth_payload_i:bitstring, SA2 : SA, TSi: traffic_selector, TSr: traffic_selector) = sdecrypt(enc_i, sk_ei) in

	let (=auth_payload_i) = MAC(hkIR,(SPI_i, undef_spi, zero, SA1, KE_i, Ni, Nr, auth_prf_i)) in

	let auth_payload_r = MAC(hkRI,(SPI_i, SPI_r, zero, SA1, KE_r,  Nr, Ni, auth_prf_r)) in
	
	let enc_r = sencrypt((R,auth_payload_r, SA2, TSi, TSr), sk_er) in
	
	event Running_R(I,R,DH_secret);
	event Running(I,R, Nr);
	out (c, (I,R,SPI_i, SPI_r, one, enc_r));

	event Commit_R(I,R,DH_secret);
	
	event Commit_aR(I,R, Nr);
	0.


	(* secret registration *)
let processfA =
        new fa:agent;
        new k1:password; 
        new k2:password;
        insert secrets(A,fa,k1);
        insert secrets(fa,A,k2).
        
let processfB =

        new fa:agent;
        new k1:password;
        new k2:password;

        insert secrets(B,fa,k1);
        insert secrets(fa,B,k2).
	
process
	new ss1 : password;
	new ss2 : password;
	insert secrets(A,B,ss1);
	insert secrets(B,A,ss2);
	
	(!initiator | !responder | !processfA| !processfB)