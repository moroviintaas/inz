/*==========================================================================*
|	Protokół IKE v2, 														|
|	INITIAL Exchange - 	wersja z uwierzytelnianiem przez MAC po obu stronach|
|						sekret jednokierunkowy, różne klucze szyfrujące		|
|	Data aktualizacji:	2018-01-15											|
|	Wykonał:			sierż. pchor. Robert Jarosz							|
|	Organizacja:		WAT, WCY, IMIK 										|
===========================================================================*/

theory ike_v2_mac
begin

builtins: diffie-hellman, hashing, symmetric-encryption
functions: prf/2, MAC/2, verifyMAC/2, MAC_key_gen/1, SK_ei/1, SK_er/1, SK_pi/1, SK_pr/1, hashkey/2


/*Użytkownicy musieli wcześniej wymienić sekret */
rule create_shared_secret:
	[Fr(~s)]
	-->
	[!Pre_shared_secret($I, $R, ~s)]

//Kompromitacja klucza sesji (sk_ei lub sk_er)
rule Reveal_session_key:
	[ !Key_exchanged(actor, sk)] --[RevKey(actor)]-> [Out(sk)]
	
//Kompromitacja wspólnego sekretu
rule compromise_shared_secret:
	[ !Pre_shared_secret(I, R, s)]
	--[
	Compromised_secret(I,R,s)
	//, Compromised_secret(R,I,s)
	]->
	[ Out(s) ]


rule reveal_state_after:
	[ !Session_stated(A,ea, dh) ]
	--[
	Compromised_session(dh)
	]->
	[ Out(ea) ]

//Obaj użytkownicy stosują MAC do uwierzytelnienia
rule I_IKE_SA_INIT:
	let
	KE_i = 'g'^~ei
	addr = <$I,$R>
	sent = <addr,~SPI_i,'0','0',~SA1, KE_i, ~Ni>
	in
	[ Fr(~Ni)
	, Fr(~ei)
	, Fr(~SPI_i)
	, Fr(~SA1)
	, !Pre_shared_secret($I, $R, s1)
	, !Pre_shared_secret($R, $I, s2)
	]
	--[
	OUT_I_INIT(sent)
	,SENT_KE(sent)
	, Starts_I($I, $R, KE_i)
	]->
	[ Out(sent)
	, I_INIT($I,$R,~SPI_i, ~SA1, ~ei, ~Ni, KE_i, s1,s2)
	
	]
	
rule R_IKE_SA_INIT:
	let
	addr = <I,R>
	KE_r = 'g'^~er
	DH_secret = KE_i^~er
	received = <addr,SPI_i,'0', '0', SA1, KE_i, Ni>
	sent = <addr,SPI_i, ~SPI_r, '0', SA1, KE_r, ~Nr>
	sk_ei = SK_ei(<Ni, ~Nr, DH_secret, SPI_i, ~SPI_r>) //aby powiązać sk_ei z dh - w celu wymuszenia secure 1 send
	sk_er = SK_er(<Ni, ~Nr, DH_secret, SPI_i, ~SPI_r>)
	in
	[ Fr(~Nr)
	, Fr(~SPI_r)
	, Fr(~er)
	, In(received)
	, !Pre_shared_secret(I, R, s1)
	, !Pre_shared_secret(R, I, s2)
	]
	--[
	IN_R_INIT(SPI_i,received)
	, Responds_R(I,R,DH_secret)
	, POINT2(I,R, DH_secret, Ni, ~Nr, SA1) 
	, Link_dh_with_sk(DH_secret, sk_ei)
	, Link_dh_with_sk(DH_secret, sk_er)
	, Link_dh_with_KE(DH_secret, KE_i)

	
	]->
	[ Out(sent)
	, R_INIT(I,R,SPI_i, ~SPI_r, SA1, DH_secret, Ni, ~Nr, KE_i, KE_r,s1,s2,~er)
	

	]
	
rule I_IKE_AUTH:
	let
	addr = <I,R>
	DH_secret = KE_r^ei
	sk_ei = SK_ei(<Ni, Nr, DH_secret, SPI_i, SPI_r>)
	sk_er = SK_er(<Ni, Nr, DH_secret, SPI_i, SPI_r>)
	prf_ipi = prf(SK_pi(<Ni, Nr, DH_secret, SPI_i, SPI_r>), I)
	received = <addr,SPI_i, SPI_r, '0', SA1, KE_r, Nr>
	hk1 = hashkey(s1, DH_secret)
	sent = <addr,SPI_i, SPI_r, '1', senc( <MAC(hk1,<addr,SPI_i,'0','0', SA1, KE_i, Ni, Nr, prf_ipi>),$SA2, $TSi, $TSr>, sk_ei)>
	in
	[
	I_INIT(I,R,SPI_i, SA1, ei, Ni, KE_i,s1,s2)
	, In(received)
	//, Fr(~SA2)
	]
	--[
	OUT_I_AUTH(sent)
	, POINT3(I,R, DH_secret, Ni, Nr, SA1)
	, Running_I(I,R, DH_secret)
	, Responds_I(I,R, DH_secret)
	, I_AUTH_SENT(MAC(hk1,<addr,SPI_i,'0','0', SA1, KE_i, Ni, Nr, prf_ipi>))
	
	]->
	[
	Out(sent)
	, I_AUTH(I,R,SPI_i, SPI_r, $SA2, DH_secret, Ni, Nr, SA1,s1,s2,ei)
	, !Key_exchanged(I, sk_ei)
	, !Key_exchanged(I, sk_er)
	
	]
	
rule R_IKE_AUTH:
	let
	hk1 = hashkey(s1, DH_secret)
	hk2 = hashkey(s2, DH_secret)
	addr = <I,R>
	prf_rpr = prf(SK_pr(<Ni, Nr, DH_secret, SPI_i, SPI_r>), R)
	auth_payload = MAC(hk,<addr,SPI_i,'0','0', SA1, KE_i, Ni, Nr, prf_ipi>)
	enc_payload = senc( <auth_payload,$SA2, $TSi, $TSr>, sk_ei)
	received = <addr,SPI_i, SPI_r, '1', enc_payload>
	sk_er = SK_er(<Ni, Nr, DH_secret, SPI_i, SPI_r>)
	sk_ei = SK_ei(<Ni, Nr, DH_secret, SPI_i, SPI_r>)
	sent = <addr,SPI_i, SPI_r, '1', senc( <MAC(hk2,<addr,SPI_i, SPI_r,'0', SA1, KE_r,  Nr,Ni, prf_rpr>), $SA2, $TSi, $TSr>, sk_er)>
	verif = MAC(hk1,<addr,SPI_i, '0','0', SA1, KE_i, Ni, Nr, prf(SK_pi(<Ni, Nr, DH_secret, SPI_i, SPI_r>), I)>)
	dc = sdec( <auth_payload,SA2, $TSi, $TSr>, SK_ei(<Ni, Nr, DH_secret, SPI_i, SPI_r>))	
	in
	[
	In(received)
	, R_INIT(I,R,SPI_i, SPI_r, SA1, DH_secret, Ni, Nr, KE_i, KE_r,s1,s2,er)
	]
	--[
	Eq(<verif, $SA2, $TSi, $TSr>, sdec(enc_payload, SK_ei(<Ni, Nr, DH_secret, SPI_i, SPI_r>)))
	, POINT4(I,R, DH_secret, Ni, Nr, SA1)
	, IN_R_AUTH($SA2, received)
	, Commit_R(I,R, DH_secret)
	, Running_R(I,R, DH_secret)
	, Commit_R_with_KE_i(I,R, DH_secret, KE_i)
	, Secret(I,R,sk_ei, DH_secret)
	, Secret(I,R,sk_er, DH_secret)
	//, R_AUTH_RECEIVED(auth_payload, DH_secret)
	//, R_AUTH_RECEIVED(auth_payload, DH_secret, sk_ei)
	, Rs_AUTH_SENT(MAC(hk2,<addr,SPI_i, SPI_r,'0', SA1, KE_r,  Nr,Ni, prf_rpr>))

	]->
	[
	Out(sent)
	, !Key_exchanged(R, sk_er)
	, !Key_exchanged(R, sk_ei)
	,  !Session_stated(R, er, DH_secret)
	]
	
rule I_FI:
	let
	hk2 = hashkey(s2, DH_secret)
	sk_ei = SK_ei(<Ni, Nr, DH_secret, SPI_i, SPI_r>)
	addr = <I,R>
	auth_payload = MAC(hk,<addr,SPI_i, SPI_r, '0', SA1, KE_r,  Nr, Ni, prf_rpr>)
	enc_payload = senc(<auth_payload, SA2, TSi, TSr>, sk_er)
	received = <addr,SPI_i, SPI_r, '1', enc_payload>
	verif = MAC(hk2,<addr,SPI_i, SPI_r, '0', SA1, KE_r,  Nr, Ni, prf(SK_pr(<Ni, Nr, DH_secret, SPI_i, SPI_r>),R)>)
	in
	[
	In(received)
	, I_AUTH(I,R,SPI_i, SPI_r, SA2, DH_secret, Ni, Nr, SA1,s1,s2,ei)
	
	]
	--[
	POINT5(I,R, DH_secret, Ni, Nr, SA1)
	, Eq(<verif, SA2, TSi, TSr>, sdec(enc_payload, SK_er(<Ni, Nr, DH_secret, SPI_i, SPI_r>)))	
	, Secret(I,R,sk_er, DH_secret)
	, Secret(I,R,sk_ei, DH_secret)
	, Commit_I(I,R, DH_secret)
	//, I_AUTH_RECEIVED(auth_payload, DH_secret)
	//, I_AUTH_RECEIVED(auth_payload, DH_secret, sk_er)
	]->
	[
	 !Session_stated(I, ei, DH_secret)
	
	]

//Ograniczenie stosowane, aby zapewnić równość w pewnych kluczowych momentach	
restriction Equal: "All x y #i. Eq(x,y) @ i ==> x = y"	


/*Lematy wykorzystywane do sprawdzenia poprawności zamodelowania protokołu - 
sprawdzenie czy może się wykonać - czy istnieje przebieg.
*/
lemma point3:
	exists-trace
	"
	Ex I R S ni nr SA1 #i #j.
		POINT3(I,R,S, ni, nr, SA1) @i & POINT2(I,R,S,ni,nr, SA1) @j
	"
	
lemma point4:
	exists-trace
	"
	Ex I R S ni nr SA1 #i #j. 
		POINT3(I,R,S, ni, nr, SA1) @i & POINT4(I,R,S,ni,nr, SA1) @j
	"

lemma point5:
	exists-trace
	"
	Ex I R S ni nr SA1 #i #j. 
	    POINT5(I,R,S, ni, nr, SA1) @i & POINT4(I,R,S,ni,nr, SA1) @j

	 "

/* Zamodelowana właściwość Injective agree inicjatora z respondentem w całości
na publicznym kanale. */
lemma int_injagree_IR:

	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			(Ex #j. Running_R(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_I(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_R(a2, r2, dh) @i3 & not(#j = #i3))
			)

			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r)
			| (Ex s #r. Compromised_secret(responder, initiator, s) @r)
			| (Ex dhx #r. Compromised_session(dhx) @r)
		)
	"
lemma ca_injagree_IR:

	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			(Ex #j. Running_R(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_I(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_R(a2, r2, dh) @i3 & not(#j = #i3))
			)

			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			//| (Ex s #r. Compromised_secret(initiator, responder, s) @r)
			| (Ex s #r. Compromised_secret(responder, initiator, s) @r)
			| (Ex #r. Compromised_session(dh) @r)
		)
	"
lemma af_injagree_IR:

	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			(Ex #j. Running_R(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_I(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_R(a2, r2, dh) @i3 & not(#j = #i3))
			)

			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r &r<i)
			| (Ex s #r. Compromised_secret(responder, initiator, s) @r &r<i)
			| (Ex dhx #r. Compromised_session(dhx) @r)
		)
	"
lemma br_injagree_IR:

	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			(Ex #j. Running_R(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_I(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_R(a2, r2, dh) @i3 & not(#j = #i3))
			)

			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r)
			| (Ex s #r. Compromised_secret(responder, initiator, s) @r)
			| (Ex #r. Compromised_session(dh) @r)
		)
	"
lemma ck_injagree_IR:

	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			(Ex #j. Running_R(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_I(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_R(a2, r2, dh) @i3 & not(#j = #i3))
			)

			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r &r<i)
			| (Ex s #r. Compromised_secret(responder, initiator, s) @r &r<i)
			| (Ex #r. Compromised_session(dh) @r)
		)
	"
lemma ca_alive_IR:
	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			((Ex A dh2 #j. Running_I(responder, A, dh2) @j & j < i)
			| (Ex A dh2 #j. Running_R(A, responder, dh2) @j & j<i)
			)
			
			| (Ex dhx #r. Compromised_session(dhx) @r)
			| (Ex s #r. Compromised_secret(responder, initiator, s) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
		)
	"	

	 
/* =================== RRR ============= III ========================*/	

lemma int_injagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			(Ex #j. Running_I(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_R(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_I(a2, r2, dh) @i3 & not(#j = #i3))
			)

			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r)
			| (Ex s #r. Compromised_secret(responder, initiator, s) @r)
			| (Ex dhx #r. Compromised_session(dhx) @r)
		)
	"
	
lemma ca_injagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			(Ex #j. Running_I(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_R(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_I(a2, r2, dh) @i3 & not(#j = #i3))
			)

			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r)
			//| (Ex s #r. Compromised_secret(responder, initiator, s) @r)
			| (Ex dhx #r. Compromised_session(dhx) @r)
		)
	"
lemma af_injagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			(Ex #j. Running_I(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_R(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_I(a2, r2, dh) @i3 & not(#j = #i3))
			)

			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r &r<i)
			| (Ex s #r. Compromised_secret(responder, initiator, s) @r &r<i)
			| (Ex dhx #r. Compromised_session(dhx) @r)
		)
	"
lemma br_injagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			(Ex #j. Running_I(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_R(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_I(a2, r2, dh) @i3 & not(#j = #i3))
			)

			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r)
			| (Ex s #r. Compromised_secret(responder, initiator, s) @r)
			| (Ex #r. Compromised_session(dh) @r)
		)
	"
lemma ck_injagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			(Ex #j. Running_I(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_R(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_I(a2, r2, dh) @i3 & not(#j = #i3))
			)

			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r &r<i)
			| (Ex s #r. Compromised_secret(responder, initiator, s) @r &r<i)
			| (Ex #r. Compromised_session(dh) @r)
		)
	"
lemma ca_alive_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex A dh2 #j. Running_I(initiator, A, dh2) @j & j < i)
			| (Ex A dh2 #j. Running_R(A, initiator, dh2) @j & j<i)
			)
			
			| (Ex dhx #r. Compromised_session(dhx) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
		)
	"	

lemma int_secret:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r)
				& not(Ex s #r. Compromised_secret(responder, initiator,  s) @r)
				& not (Ex dhx #r. Compromised_session(dhx) @r)
			)
		)
	"
lemma ca_secret_i:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret( responder, initiator, s) @r & r<i)
				& not (Ex dhx #r. Compromised_session(dhx) @r)
			)
		)
	"	
lemma ca_secret_r:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r & r<i)
				& not (Ex dhx #r. Compromised_session(dhx) @r)
			)
		)
	"

lemma af_secret:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r & r<i)
				& not(Ex s #r. Compromised_secret(responder, initiator,  s) @r & r<i)
				& not (Ex dhx #r. Compromised_session(dhx) @r)
			)
		)
	"
	
lemma br_secret:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r)
				& not(Ex s #r. Compromised_secret(responder, initiator,  s) @r)
				& not (Ex #r. Compromised_session(dh) @r)
			)
		)
	"

lemma ck_secret:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r & r<i)
				& not(Ex s #r. Compromised_secret(responder, initiator,  s) @r & r<i)
				& not (Ex #r. Compromised_session(dh) @r)
			)
		)
	"	
	

/*Poufność klucza sesji */
lemma secret_sk_e:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r & r<i)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r & r<i)
			)
		)
	"
	


end
