/*==========================================================================*
|	Protokół IKE v2, 														|
|	INITIAL Exchange - 	wersja z uwierzytelnianiem przez podpis inicjującego|
|					oraz przez MAC odpowiadającego, dodatkowy identyfiaktor	|
|					odpowiadającego w wiadomości incicjatora w IKE_AUTH,	|
|					wspólny klucz szyfrowania symetrycznego					|
|	Data aktualizacji:	2018-01-15											|
|	Wykonał:			sierż. pchor. Robert Jarosz							|
|	Organizacja:		WAT, WCY, IMIK 										|
===========================================================================*/


theory ike_v2_sigmac
begin

builtins: diffie-hellman, hashing, symmetric-encryption, asymmetric-encryption, signing
functions: prf/1, MAC/2, SK_e/1, SK_pi/1, SK_pr/1, hashkey/2

/*Użytkownicy musieli wcześniej wymienić sekret */
rule create_shared_secret:
	[Fr(~s)]
	-->
	[!Pre_shared_secret($I, $R, ~s)
	,!Pre_shared_secret($R, $I, ~s)
	]


//Rejestracja klucza publicznego
rule Register_pk:
  [ Fr(~ltkA) ]
  -->
  [ !Ltk($A, ~ltkA), !Pk($A, pk(~ltkA)), Out(pk(~ltkA)) ]
  
//Wydanie certyfikatu na klucz publiczny (wymagane by uznać klucz za właściwy)
rule Certify_pk:
	[!Pk($A, pk(ltkA)) ]
	-->
	[!Certified($CA, $A, pk(ltkA)) ]

//Kompromitacja klucza sesji (sk_ei lub sk_er)
rule Reveal_session_key:
	[ !Key_exchanged(actor, sk)] --[RevKey(actor)]-> [Out(sk)]
	
//kompromitacja klucza prywatnego
rule Reveal_ltk:
  [ !Ltk(A, ltkA) ] --[ RevLtk(A)    ]-> [ Out(ltkA) ]
  
//kmpromitacja klucza prywatnego po zakończeniu protokołu
//rule Reveal_ltk_after:
	//[!Finished_protocol_side(A,DH_secret,key), !Fi_protocol(DH_secret)] --[RevLtkAfter(A,key)]-> [ Out(key)]
	
//Kompromitacja wspólnego sekretu
rule compromise_shared_secret:
	[ !Pre_shared_secret(I, R, s)]
	--[
	Compromised_secret(I,R,s)
	,Compromised_secret(R,I,s)
	]->
	[ Out(s) ]

//Ujawnienie stanu sesji po wymianie, Ni, Nr SPI są znane nieznane są tylko DH,
//Ujawnienie DH prowadzi do Partial Dec, więc ujawniamy ei i er
rule reveal_state_after:
	[ !Session_stated(A,ea, dh) ]
	--[
	Compromised_session(dh)
	]->
	[ Out(ea) ]
	
//Inicjator używa podpisu do dowodzenia swojej tożsamości
rule I_IKE_SA_INIT:
	let
	KE_i = 'g'^~ei
	addr = <$I,$R>
	sent = <addr,~SPI_i,'0','0',~SA1, KE_i, ~Ni>
	in
	[ Fr(~Ni)
	, Fr(~ei)
	, Fr(~SPI_i)
	, Fr(~SA1)
	, !Pre_shared_secret($I, $R, s)
	]
	--[
	OUT_I_INIT(sent)
	, Starts_I($I, $R, KE_i)
	]->
	[ Out(sent)
	, I_INIT($I,$R,~SPI_i, ~SA1, ~ei, ~Ni, KE_i, s)
	]
	
rule R_IKE_SA_INIT:
	let
	addr = <I,R>
	KE_r = 'g'^~er
	DH_secret = KE_i^~er
	received = <addr,SPI_i,'0', '0', SA1, KE_i, Ni>
	sent = <addr,SPI_i, ~SPI_r, '0', SA1, KE_r, ~Nr, $CA>
	sk_ei = SK_e(<Ni, ~Nr, DH_secret, SPI_i, ~SPI_r>) //aby powiązać sk_ei z dh - w celu wymuszenia secure 1 send
	sk_er = SK_e(<Ni, ~Nr, DH_secret, SPI_i, ~SPI_r>)
	in
	[ Fr(~Nr)
	, Fr(~SPI_r)
	, Fr(~er)
	, In(received)
	, !Pre_shared_secret(I, R, s)
	]
	--[
	IN_R_INIT(SPI_i,received)
	, Responds_R(I,R,DH_secret)
	, POINT2(I,R, DH_secret, Ni, ~Nr, SA1) 
	, Link_dh_with_sk(DH_secret, sk_ei)
	, Link_dh_with_sk(DH_secret, sk_er)
	, Link_dh_with_KE(DH_secret, KE_i)
	]->
	[ Out(sent)
	, R_INIT(I,R,SPI_i, ~SPI_r, SA1, DH_secret, Ni, ~Nr, KE_i, KE_r,s,~er)
	]
	
rule I_IKE_AUTH:
	let
	addr = <I,R>
	DH_secret = KE_r^ei
	sk_ei = SK_e(<Ni, Nr, DH_secret, SPI_i, SPI_r>)
	sk_er = SK_e(<Ni, Nr, DH_secret, SPI_i, SPI_r>)
	prf_ipi = prf(SK_pi(<Ni, Nr, DH_secret, SPI_i, SPI_r>), I)
	received = <addr,SPI_i, SPI_r, '0', SA1, KE_r, Nr, CA>
	sent = <addr,SPI_i, SPI_r, '1', senc( <I,R, sign(<SPI_i,'0','0', SA1, KE_i, Ni, Nr, prf_ipi>, ltkI),pk(ltkI),$SA2, $TSi, $TSr>, sk_ei)>
	in
	[
	I_INIT(I,R,SPI_i, SA1, ei, Ni, KE_i,s)
	, In(received)
	//, Fr(~SA2)
	, !Certified(CA, I, pk(ltkI))
	//inicjator musi posiadać certyfikat klucza publicznego, aby był sens go
	//wysyłać
	]
	--[
	OUT_I_AUTH(sent)
	, POINT3(I,R, DH_secret, Ni, Nr, SA1)
	, Running_I(I,R, DH_secret)
	, Responds_I(I,R, DH_secret)	
	]->
	[
	Out(sent)
	, I_AUTH(I,R,SPI_i, SPI_r, $SA2, DH_secret, Ni, Nr, SA1, ltkI,s, KE_r,ei)
	, !Key_exchanged(I, sk_ei)
	, !Key_exchanged(I, sk_er)
	]
	
rule R_IKE_AUTH:
	let
	hk = hashkey(s, DH_secret)
	addr = <I,R>
	prf_rpr = prf(SK_pr(<Ni, Nr, DH_secret, SPI_i, SPI_r>),R)
	//auth_payload = sign(<addr,SPI_i,'0','0', SA1, KE_i, Ni, Nr, prf_ipi>, ltkI)
	enc_payload = senc( <I, R, auth_payload,pk(ltkI), $SA2, $TSi, $TSr>, sk_ei)
	received = <addr,SPI_i, SPI_r, '1', enc_payload>
	sk_er = SK_e(<Ni, Nr, DH_secret, SPI_i, SPI_r>)
	sk_ei = SK_e(<Ni, Nr, DH_secret, SPI_i, SPI_r>)
	sent = <addr,SPI_i, SPI_r, '1', senc( <R, MAC(hk,<SPI_i, SPI_r,'0', SA1, KE_r,  Nr,Ni, prf_rpr>), $SA2, $TSi, $TSr>, sk_er)>
	//verif = MAC(<addr,SPI_i, '0','0', SA1, KE_i, Ni, Nr, prf(SK_pi(<Ni, Nr, DH_secret, SPI_i, SPI_r>),I)>)
	dc = sdec( <auth_payload,SA2, $TSi, $TSr>, SK_e(<Ni, Nr, DH_secret, SPI_i, SPI_r>))	
	in
	[
	In(received)
	, R_INIT(I,R,SPI_i, SPI_r, SA1, DH_secret, Ni, Nr, KE_i, KE_r,s,er)
	, !Certified(CA, I, pk(ltkI))
	//Responder musi mieć dostęp do ceryfikatu klucza inicjatora, aby móc
	//zweryfikować podpis
	]
	--[
	Eq(verify(auth_payload, <SPI_i, '0', '0', SA1, KE_i, Ni, Nr, prf(SK_pi(<Ni, Nr, DH_secret, SPI_i, SPI_r>),I)>, pk(ltkI)), true)
	, POINT4(I,R, DH_secret, Ni, Nr, SA1)
	, IN_R_AUTH($SA2, received)
	, Commit_R(I,R, DH_secret)
	, Running_R(I,R, DH_secret)
	, Commit_R_with_KE_i(I,R, DH_secret, KE_i)
	, Secret(I,R,sk_ei, DH_secret)
	, Secret(I,R,sk_er, DH_secret)
	]->
	[
	Out(sent)
	, !Key_exchanged(R, sk_er)
	, !Key_exchanged(R, sk_ei)
	, !Session_stated(R,er, DH_secret)
	]
	
rule I_FI:
	let
	hk = hashkey(s, DH_secret)
	sk_ei = SK_e(<Ni, Nr, DH_secret, SPI_i, SPI_r>)
	addr = <I,R>
	//auth_payload = MAC(hk,<addr,SPI_i, SPI_r, '0', SA1, KE_r, Ni, Nr, prf_rpr>)
	enc_payload = senc(<R,auth_payload, SA2, TSi, TSr>, sk_er)
	received = <addr,SPI_i, SPI_r, '1', enc_payload>
	verif = MAC(hk,<SPI_i, SPI_r, '0', SA1, KE_r,  Nr,Ni, prf(SK_pr(<Ni, Nr, DH_secret, SPI_i, SPI_r>),R)>)
	in
	[
	In(received)
	, I_AUTH(I,R,SPI_i, SPI_r, SA2, DH_secret, Ni, Nr, SA1,ltkI,s, KE_r,ei)
	]
	--[
	POINT5(I,R, DH_secret, Ni, Nr, SA1)
	, Secret(I,R,sk_er, DH_secret)
	, Secret(I,R,sk_ei, DH_secret)
	, Eq(<R,verif, SA2, TSi, TSr>, sdec(enc_payload, SK_e(<Ni, Nr, DH_secret, SPI_i, SPI_r>)))
	, Commit_I(I,R, DH_secret)
	]->
	[
	!Finished_protocol_side(I,DH_secret, ltkI)
	, !Session_stated(I,ei, DH_secret)
	]
//Ograniczenie stosowane, aby zapewnić równość w pewnych kluczowych momentach	
restriction Equal: "All x y #i. Eq(x,y) @ i ==> x = y"	


/*Lematy wykorzystywane do sprawdzenia poprawności zamodelowania protokołu - 
sprawdzenie czy może się wykonać - czy istnieje przebieg.
*/
lemma point3:
	exists-trace
	"
	Ex I R S ni nr SA1 #i #j.
		POINT3(I,R,S, ni, nr, SA1) @i & POINT2(I,R,S,ni,nr, SA1) @j
	"
	
lemma point4:
	exists-trace
	"
	Ex I R S ni nr SA1 #i #j. 
		POINT3(I,R,S, ni, nr, SA1) @i & POINT4(I,R,S,ni,nr, SA1) @j
	"
lemma point5:
	exists-trace
	"
	Ex I R S ni nr SA1 #i #j. 
	    POINT5(I,R,S, ni, nr, SA1) @i & POINT4(I,R,S,ni,nr, SA1) @j
	"

/* =================== III ============= RRR ========================*/	
//Przeciwnik INT
lemma int_injagree_IR:
	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			(Ex #j. Running_R(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_I(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_R(a2, r2, dh) @i3 & not(#j = #i3))
			)
			| (Ex dhx #r. Compromised_session(dhx) @r)
			
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r)

		)
	"
	
/*Przeciwnik CA, skompromitowane ltkI*/

lemma ca_injagree_IR:
	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			(Ex #j. Running_R(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_I(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_R(a2, r2, dh) @i3 & not(#j = #i3))
			)
			| (Ex dhx #r. Compromised_session(dhx) @r)
			//| (Ex #r. RevLtk(initiator) @r) /*!*/
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r)

		)
	"

/*model AF Kompromitacja ltk po sesji możliwa */
lemma af_injagree_IR:
	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			(Ex #j. Running_R(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_I(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_R(a2, r2, dh) @i3 & not(#j = #i3))
			)
			| (Ex dhx #r. Compromised_session(dhx) @r &r<i)
			| (Ex #r. RevLtk(initiator) @r &r<i) /*!*/
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r & r<i)

		)
	"
			//wszystkie sesje poza badaną moga byc złamane
lemma br_injagree_IR:
	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			(Ex #j. Running_R(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_I(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_R(a2, r2, dh) @i3 & not(#j = #i3))
			)
			| (Ex #r. Compromised_session(dh) @r) /*!*/
			
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r)

		)
	"	
/*Przeciwnik może kompromitować inne sesje i klucze długoterminowe po
po zakończeniu atakowanej*/
lemma ck_injagree_IR:
	"
		(All initiator responder dh #i.
			Commit_I(initiator, responder, dh) @i
			==>
			(Ex #j. Running_R(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_I(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_R(a2, r2, dh) @i3 & not(#j = #i3))
			)
			| (Ex #r. Compromised_session(dh) @r) /*!*//*!*/
			| (Ex #r. RevLtk(initiator) @r& r<i) /*!*/
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
			| (Ex s #r. Compromised_secret(initiator, responder, s) @r & r<i)

		)
	"

/* =================== RRR ============= III ========================*/	
/* INJECTIVE AGREE */
	

/* INT attacker. */
lemma int_injagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			(Ex #j. Running_I(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_R(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_I(a2, r2, dh) @i3 & not(#j = #i3))
			)
			| (Ex dhx #r. Compromised_session(dhx) @r)
			
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)

		)
	"
	
lemma ca_injagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			(Ex #j. Running_I(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_R(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_I(a2, r2, dh) @i3 & not(#j = #i3))
			)
			| (Ex dhx #r. Compromised_session(dhx) @r)
			//
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)

		)
	"
lemma af_injagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			(Ex #j. Running_I(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_R(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_I(a2, r2, dh) @i3 & not(#j = #i3))
			)
			| (Ex dhx #r. Compromised_session(dhx) @r)
			| (Ex #r. RevLtk(responder) @r & r<i)
			| (Ex #r. RevLtk(initiator) @r & r<i)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)

		)
	"
lemma br_injagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			(Ex #j. Running_I(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_R(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_I(a2, r2, dh) @i3 & not(#j = #i3))
			)
			| (Ex #r. Compromised_session(dh) @r)
			
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)

		)
	"
lemma ck_injagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			(Ex #j. Running_I(initiator, responder, dh) @j & j < i
			& not (Ex a2 r2 #i2. Commit_R(a2, r2, dh) @i2 & not(#i = #i2))
			& not (Ex a2 r2 #i3. Running_I(a2, r2, dh) @i3 & not(#j = #i3))
			)
			| (Ex #r. Compromised_session(dh) @r)
			| (Ex #r. RevLtk(responder) @r & r<i)
			| (Ex #r. RevLtk(initiator) @r & r<i)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)

		)
	"
	
/* WEAK AGREE */

lemma int_weakagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex  dh2 #j. Running_I(initiator, responder, dh2) @j & j < i)
			| (Ex  dh2 #j. Running_R(responder, initiator, dh2) @j & j<i)
			)
			
			| (Ex dhx #r. Compromised_session(dhx) @r)
			
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)

		)
	"	
lemma ca_weakagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex  dh2 #j. Running_I(initiator, responder, dh2) @j & j < i)
			| (Ex  dh2 #j. Running_R(responder, initiator, dh2) @j & j<i)
			)
			
			| (Ex dhx #r. Compromised_session(dhx) @r)
			//
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)

		)
	"	
lemma af_weakagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex  dh2 #j. Running_I(initiator, responder, dh2) @j & j < i)
			| (Ex  dh2 #j. Running_R(responder, initiator, dh2) @j & j<i)
			)
			
			| (Ex dhx #r. Compromised_session(dhx) @r)
			| (Ex #r. RevLtk(responder) @r & r<i)
			| (Ex #r. RevLtk(initiator) @r & r<i)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)

		)
	"	
lemma br_weakagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex  dh2 #j. Running_I(initiator, responder, dh2) @j & j < i)
			| (Ex  dh2 #j. Running_R(responder, initiator, dh2) @j & j<i)
			)
			
			| (Ex #r. Compromised_session(dh) @r)
			
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)

		)
	"	
lemma ck_weakagree_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex  dh2 #j. Running_I(initiator, responder, dh2) @j & j < i)
			| (Ex  dh2 #j. Running_R(responder, initiator, dh2) @j & j<i)
			)
			
			| (Ex #r. Compromised_session(dh) @r)
			| (Ex #r. RevLtk(responder) @r & r<i)
			| (Ex #r. RevLtk(initiator) @r & r<i)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)

		)
	"	

/* ALIVE */	
	
lemma int_alive_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex A dh2 #j. Running_I(initiator, A, dh2) @j & j < i)
			| (Ex A dh2 #j. Running_R(A, initiator, dh2) @j & j<i)
			)
			
			| (Ex dhx #r. Compromised_session(dhx) @r)
			
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
		)
	"
lemma ca_alive_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex A dh2 #j. Running_I(initiator, A, dh2) @j & j < i)
			| (Ex A dh2 #j. Running_R(A, initiator, dh2) @j & j<i)
			)
			
			| (Ex dhx #r. Compromised_session(dhx) @r)
			//
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
		)
	"
lemma af_alive_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex A dh2 #j. Running_I(initiator, A, dh2) @j & j < i)
			| (Ex A dh2 #j. Running_R(A, initiator, dh2) @j & j<i)
			)
			
			| (Ex dhx #r. Compromised_session(dhx) @r)
			| (Ex #r. RevLtk(responder) @r & r<i)
			| (Ex #r. RevLtk(initiator) @r & r<i)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
		)
	"
lemma br_alive_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex A dh2 #j. Running_I(initiator, A, dh2) @j & j < i)
			| (Ex A dh2 #j. Running_R(A, initiator, dh2) @j & j<i)
			)
			
			| (Ex #r. Compromised_session(dh) @r)
			
			| (Ex #r. RevLtk(initiator) @r)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
		)
	"
lemma ck_alive_RI:
	"
		(All initiator responder dh #i.
			Commit_R(initiator, responder, dh) @i
			==>
			((Ex A dh2 #j. Running_I(initiator, A, dh2) @j & j < i)
			| (Ex A dh2 #j. Running_R(A, initiator, dh2) @j & j<i)
			)
			
			| (Ex #r. Compromised_session(dh) @r)
			| (Ex #r. RevLtk(responder) @r & r<i)
			| (Ex #r. RevLtk(initiator) @r & r<i)
			| (Ex #r. RevKey(responder) @r)
			| (Ex #r. RevKey(initiator) @r)
		)
	"



lemma int_secret:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r)
				& not(Ex #r. RevLtk(initiator) @r)
				& not (Ex dhx #r. Compromised_session(dhx) @r)
			)
		)
	"
lemma ca_secret_i:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r & r<i)
				& not (Ex dhx #r. Compromised_session(dhx) @r)
			)
		)
	"	
lemma ca_secret_r:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex #r. RevLtk(initiator) @r)
				& not (Ex dhx #r. Compromised_session(dhx) @r)
			)
		)
	"

lemma af_secret:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r & r<i)
				& not(Ex #r. RevLtk(initiator) @r & r<i)
				& not (Ex dhx #r. Compromised_session(dhx) @r)
			)
		)
	"
	
lemma br_secret:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r)
				& not(Ex #r. RevLtk(initiator) @r)
				& not (Ex #r. Compromised_session(dh) @r)
			)
		)
	"

lemma ck_secret:
	"
		not(Ex initiator responder key dh #i.
			(
				Secret(initiator, responder, key, dh) @i
				& (Ex #j. K(key) @j)
				& not(Ex #r. RevKey(initiator) @r)
				& not(Ex #r. RevKey(responder) @r)
				& not(Ex s #r. Compromised_secret(initiator, responder, s) @r & r<i)
				& not(Ex #r. RevLtk(initiator) @r& r<i)
				& not (Ex #r. Compromised_session(dh) @r)
			)
		)
	"	

end



